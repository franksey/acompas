export default {
    clara: {
        1: {
            src: 'clara/clara_1',
            volume: 1
        },
        2: {
            src: 'clara/clara_2',
            volume: 1
        },
        3: {
            src: 'clara/clara_3',
            volume: 0.8
        }
    },
    sorda: {
        1: {
            src: 'sorda/sorda_1',
            volume: 0.8
        },
        2: {
            src: 'sorda/sorda_2',
            volume: 0.8
        },
        3: {
            src: 'sorda/sorda_3',
            volume: 0.8
        }
    },
    cajon: {
        1: {
            src: 'cajon/cajon_1',
            volume: 1
        },
        2: {
            src: 'cajon/cajon_2',
            volume: 1
        },
        3: {
            src: 'cajon/cajon_3',
            volume: 1
        }
    },
    udu: {
        1: {
            src: 'udu/udu_1',
            volume: 1
        },
        2: {
            src: 'udu/udu_2',
            volume: 0.8
        }
    },
    jaleo: {
        1: {
            src: 'jaleo/jaleo_1',
            volume: 0.8
        },
        2: {
            src: 'jaleo/jaleo_2',
            volume: 0.8
        },
        3: {
            src: 'jaleo/jaleo_3',
            volume: 0.8
        },
        4: {
            src: 'jaleo/jaleo_4',
            volume: 0.8
        },
        5: {
            src: 'jaleo/jaleo_5',
            volume: 0.8
        },
        6: {
            src: 'jaleo/jaleo_6',
            volume: 0.8
        },
        7: {
            src: 'jaleo/jaleo_7',
            volume: 0.8
        },
        8: {
            src: 'jaleo/jaleo_8',
            volume: 0.8
        },
        9: {
            src: 'jaleo/jaleo_9',
            volume: 0.8
        },
        10: {
            src: 'jaleo/jaleo_10',
            volume: 0.8
        },
        11: {
            src: 'jaleo/jaleo_11',
            volume: 0.8
        },
        12: {
            src: 'jaleo/jaleo_12',
            volume: 0.8
        },
        13: {
            src: 'jaleo/jaleo_13',
            volume: 0.8
        },
        14: {
            src: 'jaleo/jaleo_14',
            volume: 0.8
        },
        15: {
            src: 'jaleo/jaleo_15',
            volume: 0.8
        },
        16: {
            src: 'jaleo/jaleo_16',
            volume: 0.8
        },
        17: {
            src: 'jaleo/jaleo_17',
            volume: 0.8
        }
    },
    click: {
        1: {
            src: 'click/click_1',
            volume: 0.2
        },
        2: {
            src: 'click/click_2',
            volume: 0.2
        }
    }
}
